import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

/**
 * Generated class for the CommentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-comment",
  templateUrl: "comment.html"
})
export class CommentPage {
  comment: FormGroup;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private formBuilder: FormBuilder
  ) {
    this.comment = this.formBuilder.group({
      name: ["", Validators.required],
      range: 5,
      comment: ["", Validators.required],
      date: new Date().toISOString()
    });
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad CommentPage");
  }
  onSubmit() {
    this.viewCtrl.dismiss({ comment: this.comment });
  }
}
