import { SocialSharing } from "@ionic-native/social-sharing";
import { CommentPage } from "./../comment/comment";
import { Component, Inject } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  ActionSheetController,
  ModalController
} from "ionic-angular";
import { Dish } from "../../shared/dish";
import { FavoriteProvider } from "../../providers/favorite/favorite";

/**
 * Generated class for the DishdetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: "page-dishdetail",
  templateUrl: "dishdetail.html"
})
export class DishdetailPage {
  favorite: boolean;

  dish: Dish;
  errMess: string;
  avgstars: string;
  numcomments: number;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private toastCtrl: ToastController,
    private actionSheetCtrl: ActionSheetController,
    @Inject("BaseURL") private BaseURL,
    private favoriteservice: FavoriteProvider,
    private socialSharing: SocialSharing
  ) {
    this.dish = navParams.get("dish");
    this.favorite = favoriteservice.isFavorite(this.dish.id);
    this.numcomments = this.dish.comments.length;
    let total = 0;
    this.dish.comments.forEach(comment => (total += comment.rating));
    this.avgstars = (total / this.numcomments).toFixed(2);
  }
  btnClicked() {
    let action = this.actionSheetCtrl.create({
      title: "Select Actions",
      buttons: [
        {
          text: "Add to Favotites",
          handler: () => {
            this.addToFavorites();
          }
        },
        {
          text: "Add Comment",
          handler: () => {
            let modal = this.modalCtrl.create(CommentPage);
            modal.onDidDismiss(com => {
              if (com) {
                let comment = com.comment.value;
                console.log(comment.comment.value);
                let newComment = {
                  rating: comment.range,
                  comment: comment.comment,
                  author: comment.name,
                  date: comment.date
                };
                this.dish.comments.push(newComment);
              }
            });
            modal.present();
          }
        },
        {
          text: "Share via Facebook",
          handler: () => {
            this.socialSharing
              .shareViaFacebook(
                this.dish.name + " -- " + this.dish.description,
                this.BaseURL + this.dish.image,
                ""
              )
              .then(() => console.log("Posted successfully"))
              .catch(() => console.log("Failed to post"));
          }
        },
        {
          text: "Share via Twitter",
          handler: () => {
            this.socialSharing
              .shareViaTwitter(
                this.dish.name + " -- " + this.dish.description,
                this.BaseURL + this.dish.image,
                ""
              )
              .then(() => console.log("Posted successfully to Twitter"))
              .catch(() => console.log("Failed to post to Twitter"));
          }
        },
        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });
    action.present();
  }
  addToFavorites() {
    console.log("Adding to Favorites", this.dish.id);
    this.favorite = this.favoriteservice.addFavorite(this.dish.id);
    this.toastCtrl
      .create({
        message: "Dish " + this.dish.id + " added as a favorite successfully",
        position: "middle",
        duration: 3000
      })
      .present();
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad DishdetailPage");
  }
}
